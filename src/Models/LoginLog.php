<?php

namespace LaraExt\LoginLog\Models;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{

    protected $table = 'login_log';
    //protected $connection = 'mysql1';
    //public $timestamps = false;
    public function selectCount($where = false, $join = false, $group = false)
    {
        $obj = $this;
        if ($where) {
            if (isset($where['where'])) {
                foreach ($where['where'] as $k => $v) {
                    $obj = $obj->where($v[0], $v[1], $v[2]);
                }
            }
            if (isset($where['whereBetween'])) {
                foreach ($where['whereBetween'] as $k => $v) {
                    //$v[0]是字段，$v[1]是数组，实例[1,6];
                    $obj = $obj->whereBetween($v[0], $v[1]);
                }
            }
            if (isset($where['whereIn'])) {
                foreach ($where['whereIn'] as $k => $v) {
                    //$v[0]是字段，$v[1]是数组，实例[1,6];
                    $obj = $obj->whereIn($v[0], $v[1]);
                }
            }
            if (isset($where['orWhere'])) {
                foreach ($where['orWhere'] as $k => $v) {
                    $obj = $obj->orWhere($v[0], $v[1], $v[2]);
                }
            }
            if (isset($where['whereRaw'])) {
                foreach ($where['whereRaw'] as $k => $v) {
                    $obj = $obj->whereRaw($v);
                }
            }
        }
        if ($join) {
            foreach ($join as $k => $v) {
                if (is_array($v[0])) {
                    foreach ($v as $vv) {
                        $obj = $obj->$k($vv[0], $vv[1], $vv[2], $vv[3]);
                    }
                } else {
                    $obj = $obj->$k($v[0], $v[1], $v[2], $v[3]);
                }
            }
        }
        if ($group) {
            $obj = $obj->groupBy($group);
        }
        $num = $obj->count();
        return $num;
    }
    public function getObj()
    {
        return $this;
    }
    public function selectAll($select = false, $where = false, $order = false, $limit = false, $join = false, $group = false, $having = false)
    {
        $obj = $this->getObj();
        if ($select) {
            $obj = $this->select($select);
        }
        if ($where) {
            if (isset($where['where'])) {
                foreach ($where['where'] as $k => $v) {
                    $obj = $obj->where($v[0], $v[1], $v[2]);
                }
            }
            if (isset($where['whereBetween'])) {
                foreach ($where['whereBetween'] as $k => $v) {
                    //$v[0]是字段，$v[1]是数组，实例[1,6];
                    $obj = $obj->whereBetween($v[0], $v[1]);
                }
            }
            if (isset($where['whereIn'])) {
                foreach ($where['whereIn'] as $k => $v) {
                    //$v[0]是字段，$v[1]是数组，实例[1,6];
                    $obj = $obj->whereIn($v[0], $v[1]);
                }
            }
            if (isset($where['orWhere'])) {
                foreach ($where['orWhere'] as $k => $v) {
                    $obj = $obj->orWhere($v[0], $v[1], $v[2]);
                }
            }
            if (isset($where['whereRaw'])) {
                foreach ($where['whereRaw'] as $k => $v) {
                    $obj = $obj->whereRaw($v);
                }
            }
        }
        if ($join) {
            foreach ($join as $k => $v) {
                if (is_array($v[0])) {
                    foreach ($v as $vv) {
                        $obj = $obj->$k($vv[0], $vv[1], $vv[2], $vv[3]);
                    }
                } else {
                    $obj = $obj->$k($v[0], $v[1], $v[2], $v[3]);
                }
            }
        }

        if ($order) {
            foreach ($order as $k => $v) {
                $obj = $obj->orderBy($k, $v);
            }
        }
        if ($group) {
            $obj = $obj->groupBy($group);
        }
        if ($limit) {
            $skip = ($limit['page'] - 1) * $limit['limit'];
            $obj = $obj->offset($skip)->limit($limit['limit']);
        }

        $data = $obj->get()->toArray();
        return $data;
    }
}
